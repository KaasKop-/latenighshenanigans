<?php

namespace LNS\Helpers;
include_once("DbHelper.php");

/**
 * Class EncryptionHelper
 * @package LNS\Helpers
 */
class EncryptionHelper
{
    protected $imageName;
    protected $dbConn;
    protected $user;

    public function __construct()
    {
        $this->dbConn = new dbHelper();
        $this->dbConn->connect("pdo", "mysql", "localhost", "root", "kaas", "LNS");
    }

    /**
     * @param string $fileToEncrypt
     * @param string $fileName
     * @param string $userKey
     * @return string
     * @throws \Exception
     */
    public function encryptFile($fileToEncrypt, $fileName, $userKey)
    {

        try {
            $key = random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES);
            $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        } catch (\Exception $e) {
            throw new \Exception("ERROR: " . $e->getMessage());
        }

        $this->dbConn->insert("images", ["image_name" => $fileName, "image_nonce" => bin2hex($nonce), "image_key" => bin2hex($key), "uploaded_by" => $userKey]);
        $imageEncrypted = sodium_crypto_secretbox(base64_encode(file_get_contents($fileToEncrypt)), $nonce, $key);
        return $imageEncrypted;
    }

    /**
     * @param string $fileToDecrypt
     * @return bool|string
     * @throws \Exception
     */
    public function decryptFile($fileToDecrypt)
    {
        $fileArray = explode("/", $fileToDecrypt);
        $imageName = end($fileArray);

        if (strlen($imageName) >= 10 and strlen($imageName) <= 35) {
            try {
                $image_data = $this->dbConn->fetch_row("SELECT * FROM images WHERE image_name = ?", $imageName);
                $key = hex2bin($image_data["image_key"]);
                $nonce = hex2bin($image_data["image_nonce"]);
                $imageDecrypted = sodium_crypto_secretbox_open(file_get_contents($fileToDecrypt), $nonce, $key);
            } catch (\Exception $e) {
                throw new \Exception("ERROR" . $e->getMessage());
            }
            return $imageDecrypted;
        }
        return false;
    }
}
