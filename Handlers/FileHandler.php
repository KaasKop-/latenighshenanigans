<?php

namespace LNS\Handlers;
include_once("../Helpers/EncryptionHelper.php");
include_once("../Helpers/DbHelper.php");
include_once("../Handlers/User.php");

use LNS\Helpers\EncryptionHelper;
use LNS\Handlers\User;


/**
 * Class FileHandler
 * @package LNS\Handlers
 */
class FileHandler
{
    private $storageDirectory = "/home/mitch/Projects/PHP/LNS/Testing/";
    private $encryptionHelper;
    private $user;

    public function __construct()
    {
        $this->encryptionHelper = new EncryptionHelper();
        $this->user = new User();
    }

    /**
     * @param string $imageLocation
     * @param string $userKey
     * @throws \Exception
     * @return bool
     * Stores the file after encryption.
     * Encryption is done using sodium
     */
    public function storeFile($imageLocation, $userKey)
    {
        $uniqueName = bin2hex(random_bytes(random_int(10, 15)));
        try {
            $encryptedImage = $this->encryptionHelper->encryptFile($imageLocation, $uniqueName, $userKey);
            file_put_contents($this->storageDirectory . $uniqueName, $encryptedImage);
        } catch (\Exception $e) {
            throw new \Exception("ERROR" . $e->getMessage());
        }


        if ($encryptedImage === false) {
            print("ERROR: Encryption failed");
            return false;
        }
        return true;
    }

    /**
     * @param string image_name
     * @return bool|array
     * @throws \Exception
     */
    public function fetchFile($fileName)
    {
        $fullPathToImage = $this->storageDirectory . $fileName;
        $imageEncrypted = $this->encryptionHelper->decryptFile($fullPathToImage);

        if (empty($imageEncrypted) or is_null($imageEncrypted)) {
            print("Something went wrong while decrypting the image.");
            return false;
        }
        return $imageEncrypted;
    }
}
