<?php

namespace LNS\Handlers;

include_once("../Helpers/DbHelper.php");

use LNS\Helpers\dbHelper;

class User
{
    private $dbConnection;

    public function __construct()
    {
        $this->dbConnection = new dbHelper();
        $this->dbConnection->connect("pdo", "mysql", "localhost", "root", "kaas", "LNS");
    }

    public function userExists($key)
    {
        $userData = $this->dbConnection->fetch_row("SELECT user_key FROM users WHERE user_key = ?", $key);
        if (!empty($userData)) {
            return true;
        }
        return false;
    }

    /**
     * @param $filename
     * @throws \Exception
     * @return string
     */
    public function getUsernameFromFileName($filename)
    {
        $userData = $this->dbConnection->fetch_row("SELECT users.user_name FROM images INNER JOIN users ON images.uploaded_by=users.user_key WHERE images.image_name = ?", $filename);
        return $userData["user_name"];
    }

    public function getImageListFromKey($userKey)
    {
        //TODO Validation
        $userImages = $this->dbConnection->fetch_all("SELECT * FROM images WHERE uploaded_by = ?", $userKey);
        return $userImages;
    }

    public function registerNewUser($username, $password)
    {

    }
}