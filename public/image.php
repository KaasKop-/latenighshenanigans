<?php
    include_once("../Handlers/FileHandler.php");
    include_once("../Handlers/User.php");
    use LNS\Handlers\FileHandler;
    use LNS\Handlers\User;

    $file_requested = explode("/", $_SERVER["REQUEST_URI"]);

    //TODO Find a better way to circumvent SQL injection here.
    if(!empty($file_requested[2]) and strlen($file_requested[2]) === strlen(substr($file_requested[2], strpos($file_requested[2], "%"))))
    {
        $fileHandler = new FileHandler();
        $user = new User();
        $user_name = $user->getUsernameFromFileName($file_requested[2]);
        $image_data = $fileHandler->fetchFile($file_requested[2]);
    }
    else
    {
        $image_data = file_get_contents("../Testing/error");
    }
?>

<html>
<head>
    <title>Late Night Shenanigans</title>
    <style>
        body
        {
            background-color: #212121;
            color: #dfdfdf;
            margin: auto;
            text-align: center;
        }

        img
        {
            max-width:100%;
            max-height:100%;
        }
    </style>
</head>
<body>
<p>Image uploaded by: <?php echo $user_name ?></p>
<img src="data:image/png;base64,<?php echo $image_data ?>"/>
</body>
</html>
