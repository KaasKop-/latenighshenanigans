<?php
include_once("../Handlers/FileHandler.php");
include_once("../Handlers/User.php");

use LNS\Handlers\FileHandler;
use LNS\Handlers\User;

if (isset($_POST) && !empty($_POST) && isset($_FILES)) {
    // This means the form has been filled out properly and we can now start encrypting it and serving the user the image.
    // tmp_location in the future will be $_FILES["file"]["tmp_location"] which is where the uploaded file is placed for the server. usually resides in /tmp.
    $fileHandler = new FileHandler();
    $user = new User();
    if ($user->userExists($_POST["api_key"])) {
        $fileHandler->storeFile($_FILES["file"]["tmp_name"], $_POST["api_key"]);
    } else {
        $error = "ERROR: Key does not match";
    }

}
?>

<html>
<head>
    <title>Late Night Shenanigans</title>
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
</head>
<body>
<form id="uploadForm" enctype="multipart/form-data" action="index.php" method="post">
    <input id="file" type="file" name="file" required="required"/>
    <input id="api_key" type="text" name="api_key"/>
    <input type="submit"/>
</form>
<?php (isset($error)) ? print($error) : ""; ?>
</body>
</html>
