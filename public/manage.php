<?php
include_once("../Handlers/User.php");
include_once("../Handlers/FileHandler.php");

use LNS\Handlers\User;
use LNS\Handlers\FileHandler;

if (isset($_POST) and !empty($_POST)) {
    $user = new User();
    $fileHandler = new FileHandler();
    $imagesFromUser = $user->getImageListFromKey($_POST["api_key"]);
}
?>

<html>
<head>
    <title>LNS Manage images</title>
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
</head>
<body>
<form id="uploadForm" action="manage.php" method="post">
    <input id="api_key" type="text" name="api_key"/>
    <input type="submit"/>
</form>

<?php
if (isset($_POST["api_key"])) {
    for ($i = 0; $i < count($imagesFromUser); $i++) {
        print("<img src='data:image/png;base64," . $fileHandler->fetchFile($imagesFromUser[$i]["image_name"]) . "'/>");
    }
}
?>
</body>
</html>
